package com.qhz;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Deal {
    private String path;

    public Deal(String path) {
        this.path = path;
    }

    public List<OneRow> readFile() throws Exception{
        File file = new File(path);
        InputStream in = new FileInputStream(file);
        XSSFWorkbook sheets = new XSSFWorkbook(in);

        XSSFSheet sheetAt = sheets.getSheetAt(0);

        ArrayList<OneRow> list = new ArrayList<>();
        // 循环获取每一行数据
        for (int i = 1; i < sheetAt.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = sheetAt.getRow(i);
            OneRow oneRow = new OneRow();
            oneRow.setId(i);
            //设置open
            XSSFCell close = row.getCell(3);
            if(close!=null && CellType.NUMERIC.equals(close.getCellType()) ){
                double numericCellValue = close.getNumericCellValue();
                oneRow.setClose(new BigDecimal(numericCellValue).setScale(2, BigDecimal.ROUND_HALF_UP));
            }
            //设置方向
            XSSFCell openAPosition = row.getCell(7);
            if(openAPosition!=null && CellType.STRING.equals(openAPosition.getCellType()) ){
                String stringCellValue = openAPosition.getStringCellValue();
                oneRow.setDirection(stringCellValue);
            }
            //设置开仓价格
            XSSFCell closeAPosition = row.getCell(8);
            if(closeAPosition!=null && CellType.FORMULA.equals(closeAPosition.getCellType())){
                double numericCellValue = closeAPosition.getNumericCellValue();
                oneRow.setOpenAPosition(new BigDecimal(numericCellValue).setScale(2, BigDecimal.ROUND_HALF_UP));
            }
            list.add(oneRow);
        }
        in.close();
        System.out.println("共"+list.size()+"条数据");
        return list;
    }

    public List<OneRow> calculation(List<OneRow> data){
        ArrayList<OneRow> oneRows = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            OneRow oneRow = data.get(i);
            //获取方向
            String direction = oneRow.getDirection();
            if(direction!=null){
                if (direction.equals("空")) {
                    //拿到开仓价
                    BigDecimal openAPosition = oneRow.getOpenAPosition();
                    BigDecimal multiply = openAPosition.multiply(new BigDecimal(0.99).setScale(2, BigDecimal.ROUND_HALF_UP));
                    for (int j = i+1; j < data.size(); j++) {

                        OneRow oneRow1 = data.get(j);
                        String direction1 = oneRow1.getDirection();
                        if(direction1 ==null || direction1.equals("空")){
                            if(oneRow1.getClose().compareTo(multiply) < 1){
                                oneRow.setCloseAPosition(multiply.setScale(2,BigDecimal.ROUND_HALF_UP));
                                oneRows.add(oneRow);
                                i=j;
                                break;
                            }
                        }else {
                            i=j-1;
                            oneRow.setCloseAPosition(new BigDecimal(0).setScale(2,BigDecimal.ROUND_HALF_UP));
                            oneRows.add(oneRow);
                            break;
                        }
                    }
                }else if(direction.equals("多")){
                    for (int j = i+1; j < data.size(); j++) {
                        OneRow oneRow1 = data.get(j);
                        String direction1 = oneRow1.getDirection();
                        if(direction1 !=null && direction1.equals("空")){
                            oneRow.setCloseAPosition(oneRow1.getOpenAPosition().setScale(2,BigDecimal.ROUND_HALF_UP));
                            oneRows.add(oneRow);
                            i=j-1;
                            break;
                        }
                    }
                }
            }
        }
        return oneRows;
    }

    public void writeFiel(List<OneRow> data) throws Exception {
        File file = new File(path);
        InputStream in = new FileInputStream(file);
        XSSFWorkbook sheets = new XSSFWorkbook(in);
        XSSFSheet sheetAt = sheets.getSheetAt(0);
        for (int i = 0; i < data.size(); i++) {
            OneRow oneRow = data.get(i);
            XSSFRow row = sheetAt.getRow(oneRow.getId());
            XSSFCell cell = row.getCell(9);
            if(cell == null ){
                cell = row.createCell(9);
            }
            sheetAt.setColumnWidth(9,3000);
            cell.setCellType(CellType.NUMERIC);
            cell.setCellValue(oneRow.getCloseAPosition().setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
        }
        FileOutputStream excelFileOutPutStream = new FileOutputStream(file);
        sheets.write(excelFileOutPutStream);
        excelFileOutPutStream.flush();
        excelFileOutPutStream.close();
    }
}
