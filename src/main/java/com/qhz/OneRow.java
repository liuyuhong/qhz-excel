package com.qhz;

import java.math.BigDecimal;

public class OneRow {
    private Integer id;
    private BigDecimal close;
    private BigDecimal openAPosition;
    private BigDecimal closeAPosition;
    private String direction;

    public BigDecimal getClose() {
        return close;
    }

    public void setClose(BigDecimal close) {
        this.close = close;
    }

    public BigDecimal getOpenAPosition() {
        return openAPosition;
    }

    public void setOpenAPosition(BigDecimal openAPosition) {
        this.openAPosition = openAPosition;
    }

    public BigDecimal getCloseAPosition() {
        return closeAPosition;
    }

    public void setCloseAPosition(BigDecimal closeAPosition) {
        this.closeAPosition = closeAPosition;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "OneRow{" +
                "id=" + id +
                ", close=" + close +
                ", openAPosition=" + openAPosition +
                ", closeAPosition=" + closeAPosition +
                ", direction='" + direction + '\'' +
                '}';
    }

    public OneRow(Integer id, BigDecimal close, BigDecimal openAPosition, BigDecimal closeAPosition, String direction) {
        this.id = id;
        this.close = close;
        this.openAPosition = openAPosition;
        this.closeAPosition = closeAPosition;
        this.direction = direction;
    }

    public OneRow() {
    }
}
