package com.qhz;

import java.io.File;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        String path=System.getProperty("user.dir");
        System.out.println("当前目录 = " + path);
        File file = new File(path);
        File[] files = file.listFiles();
        if (files.length > 0) {
            for (int i = 0; i < files.length; i++) {
                File file1 = files[i];
                if(file1.getName().endsWith(".xlsx")){
                    Deal deal = new Deal(file1.getPath());
                    List<OneRow> oneRows = deal.readFile();
                    List<OneRow> calculation = deal.calculation(oneRows);
                    deal.writeFiel(calculation);
                }
            }
        }
    }
}
